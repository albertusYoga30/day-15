const form = document.getElementById("main-form");

form.addEventListener("submit", (event) => {
  var username = document.getElementById("username").value;
  var password = document.getElementById("password").value;
  var confirmPass = document.getElementById("confirm-password").value;
  var email = document.getElementById("email").value;
  var confirmEmail = document.getElementById("confirm-email").value;

  if (username == "") {
    document.getElementById("usernameHelp").style.display = "block";
    return false;
  } else if (username.length < 6) {
    document.getElementById("usernameHelp").style.display = "block";
    document.getElementById("usernameHelp").innerText = "a minimum of 6 characters in length";
    return false;
  } else if (username != "" && username.length >= 6) {
    document.getElementById("usernameHelp").style.display = "none";
  }

  var expression = /^(?=.*[0-9])[a-z]|(?=.*[a-z])[0-9]/;
  if (password == "") {
    document.getElementById("passHelp").style.display = "block";
    return false;
  } else if (expression.test(password) == false) {
    document.getElementById("passHelp").style.display = "block";
    document.getElementById("passHelp").innerText = "Passwords must contain [0-9] and [a-z]";
    return false;
  } else if (expression.test(password) == true) {
    document.getElementById("passHelp").style.display = "none";
  }

  if (confirmPass == "") {
    document.getElementById("passConfr").style.display = "block";
    return false;
  } else if (confirmPass == password) {
    document.getElementById("passConfr").style.display = "none";
  } else if (confirmPass != password) {
    document.getElementById("passConfr").style.display = "block";
    document.getElementById("passConfr").innerText = "password is not match!";
    return false;
  }

  if (email == "") {
    document.getElementById("emailHelp").style.display = "block";
    return false;
  } else if (email != "") {
    document.getElementById("emailHelp").style.display = "none";
  }

  if (confirmEmail == "") {
    document.getElementById("emailconfr").style.display = "block";
    return false;
  } else if (confirmEmail == email) {
    document.getElementById("emailconfr").style.display = "none";
  } else if (confirmEmail != email) {
    document.getElementById("emailconfr").style.display = "block";
    document.getElementById("emailconfr").innerText = "email is not match!";
    return false;
  }

  alert("Submit berhasil");
  event.preventDefault();
});
